import requests
import os
import json

#api endpoints for datasources and dashboards
endpoint_datasource = 'http://admin:admin@172.19.0.15:3000/api/datasources'
endpoint_dashboards = 'http://admin:admin@172.19.0.15:3000/api/dashboards/import'
headers = {'content-type': 'application/json'}

#paths for dashboards and datasources
path_datasource = 'datasources/'
path_dashboard = 'dashboards/'

#Read and send datasource config to grafana
files = os.listdir(path_datasource)
print('Pushing datasource information to grafana...')
for file in files:
    if 'datasource_' in file:
     f = open(path_datasource+file, 'r')
     data = f.read()
     r = requests.post(endpoint_datasource,data,headers=headers)
     print(r)
     #r.json()
     f.close()

#Read and send dashboard config to grafana
print('Pushing dashboard information to grafana...')
files = os.listdir(path_dashboard)
for file in files:
    f = open(path_dashboard+file, 'r')
    data = f.read()
    r = requests.post(endpoint_dashboards,data,headers=headers)
    print(r)
    #r.json()
    f.close()
