from prometheus_client import Gauge
import time
import random
#from prometheus_client.core import CollectorRegistry
from prometheus_client import start_http_server


#Metrics definition (name and description)
sau_kpi = Gauge("SAU","SAU KPI")
sau_kpi.set(2227855)


def calculate_sau(t):
    time.sleep(t)
    sau_kpi.inc()

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        calculate_sau(random.random())
